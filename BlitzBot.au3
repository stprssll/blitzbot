;Bejewled Blitz Bot
;Operated via Facebook on Mozilla firefox
; Control Coords x, y from top left
;
; Control Height       320
; Control Width        320
; Play Board           8 x 8
; Play Cells           40 x 40

;------------------------------------------------------------
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
;
const $MAX_COL = 9
Global $Active = False

;Color bits guide
; There are three group of 8 bits of color - RGB
; The values are rated into three intensities Low - 012345 Medium - 6789A High - BCDEF
; This is done with reference to the first digit in each pair

Global $color[$MAX_COL][2] = [ _
["HHL", "Y"], _
["LHH", "B"], _
["LMH", "B"], _
["LLL", "B"], _
["HHH", "W"], _
["HML", "O"], _
["HLH", "P"], _
["LHL", "G"], _
["HLL", "R"] _
]

const $GRID_SZ_X = 8
const $GRID_SZ_Y = 8

global $grid[$GRID_SZ_X][$GRID_SZ_Y]	;Base colour
global $spec[$GRID_SZ_X][$GRID_SZ_Y]	;Special attributes
										;N-Normal, F_Flame, C-Coin, X-Spinning Cube

Global $move[100][5]	; From: x,y To: x,y Rank: 345
Global $last_move[5]			; last move - detect retries
Global $move_idx = 0	; index into above table
global $move_count = 0	; how many moves in loop
global $bad_recon = 0	; count of bad scans
global $special_flag = "N"

Global $test_grid1 =	"YBYBYBYB" & _
						"PGRGRGPG" & _
						"YBYRYBYB" & _
						"PGPGPGPG" & _
						"YBRRYRYB" & _
						"PGPGRGPG" & _
						"YBYBYBYB" & _
						"PGPGPGPG"

Global $test_spec1 =	"NNNNNNNN" & _
						"NNNNNNNN" & _
						"NNNFNNNN" & _
						"NNNNNNNN" & _
						"NNNNNNNN" & _
						"NNNNNNNN" & _
						"NNNNNNNN" & _
						"NNNNNNNN"

Global $hwnd = 0
Global $control_x = 0
Global $control_y = 0

Global $offset_x = 2
global $offset_y = -2

global $step_fwd_x = 0
global $step_fwd_y = 0
global $step_left_x = 0
global $step_left_y = 0
global $step_right_x = 0
global $step_right_y = 0

global $x = 0
global $y = 0

global $mouse_x = 0
global $mouse_y = 0			;Detect manual mouse move

Const $PLAY_X = 172
const $PLAY_Y = 62

const $CELL_HEIGHT = 40
const $CELL_WIDTH = 40

const $WINDOW = "Bejeweled Blitz on Facebook - Mozilla Firefox"
const $CONTROLID = "[CLASS:MozillaWindowClass; INSTANCE:9]"

;Use controls coordinates
Opt("CaretCoordMode", 0)        ;1=absolute, 0=relative, 2=client
Opt("MouseCoordMode", 0)        ;1=absolute, 0=relative, 2=client

#Region ### START Koda GUI section ### Form=D:\My Stuff\My Autoit\BlitzBot\BlitzBot.kxf
$Form1_1 = GUICreate("Form1", 390, 315, 192, 124)
$Button1 = GUICtrlCreateButton("Start", 8, 280, 73, 25, $WS_GROUP)
$Button2 = GUICtrlCreateButton("Test", 88, 280, 65, 25, 0)
$Grid_out = GUICtrlCreateLabel("", 8, 8, 124, 132, $WS_BORDER)
$Spec_out = GUICtrlCreateLabel("", 152, 8, 132, 132, $WS_BORDER)
$ctrl_offset_x = GUICtrlCreateInput("4", 320, 32, 57, 21)
$ctrl_offset_y = GUICtrlCreateInput("4", 320, 56, 57, 21)
$Label1 = GUICtrlCreateLabel("Offsets", 320, 8, 37, 17)
$Label2 = GUICtrlCreateLabel("X", 304, 32, 11, 17)
$Label3 = GUICtrlCreateLabel("Y", 304, 56, 11, 17)
$Log = GUICtrlCreateLabel("", 8, 256, 380, 28)

GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

init_game()


While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $Button1
			$Active = True
			$move_count = 0
			$mouse_x = 0
			GUICtrlSetData($Button1, "Stop")
		Case $Button2
			;Test play space position sensing
			WinActivate($WINDOW)
			If not WinActive($WINDOW) Then
				GUICtrlSetData($Grid_out, "No Game")
			else
				MouseMove($control_x + $PLAY_X, $control_y + $PLAY_Y, 10)
			endif

		Case Else
			if $Active then
				$offset_x = GUICtrlRead($ctrl_offset_x)
				$offset_y = GUICtrlRead($ctrl_offset_y)

				play_move()
				if not $active Then
					GUICtrlSetData($Button1, "Start")
				endif
				$move_count = $move_count + 1
			EndIf
	EndSwitch
WEnd

Exit	;Safe Termination

func play_move()
	WinActivate($WINDOW)
	If not WinActive($WINDOW) Then
		Log("Window is not active")
		$active = False
	Else
		sample_grid()
		;load_grid($test_grid1)
		print_grid()
		print_spec()

		;Logmsg("Bad cells = " & $bad_recon)

		if $bad_recon < 5 then
			find_move()
			;print_move()
			Logmsg("Moves = " & $move_idx)
			do_move()
			sleep(500)
		Else
			$active = False
		endif
	endif
EndFunc

;--------------------------------------------------------------------

func Logmsg($s)
GUICtrlSetData($Log, $s)
EndFunc

func do_move()
;Use the $move list to click, move, release mouse over app
;Use the first offered move of highest rank

;Detect manual mouse move
if $mouse_x > 0 then
	$pos = MouseGetPos()
	if (abs($pos[0]-$mouse_x) + abs($pos[1]-$mouse_y)) > 5 then
		$Active = False
		return
	endif
EndIf

$found = false

do
	$curr_rank = 0
	$curr_move = 0
	for $i = 0 to $move_idx-1
		if $move[$i][4] > $curr_rank Then
			$curr_rank = $move[$i][4]
			$curr_move = $i
		endif
	next

	$x1 = $move[$curr_move][0]	;From x
	$y1 = $move[$curr_move][1]	;From y
	$x2 = $move[$curr_move][2]	;To x
	$y2 = $move[$curr_move][3]	;To y

	;Detect retries
	if $last_move[0] = $x1 _
	and $last_move[1] = $y1 _
	and $last_move[2] = $x2 _
	and $last_move[3] = $y2 Then
		$move[$curr_move][4] = 0	; kill the move
		$found = false
	Else
		$last_move[0] = $x1
		$last_move[1] = $y1
		$last_move[2] = $x2
		$last_move[3] = $y2
		$found = true
	endif
until $found

;Move
logmsg("Move rank = " & $curr_rank)

$mouse_x = $control_x + $PLAY_X + ($x1 * $CELL_WIDTH) + ($CELL_WIDTH/2)
$mouse_y = $control_y + $PLAY_Y + ($y1 * $CELL_HEIGHT) + ($CELL_HEIGHT/2)
MouseMove($mouse_x, $mouse_y, 10)
MouseDown("left")
$mouse_x = $control_x + $PLAY_X + ($x2 * $CELL_WIDTH) + ($CELL_WIDTH/2)
$mouse_y = $control_y + $PLAY_Y + ($y2 * $CELL_HEIGHT) + ($CELL_HEIGHT/2)
MouseMove($mouse_x, $mouse_y, 10)
mouseup("left")

EndFunc

func find_move()
;Search for play patterns in the grid
; Patterns are of the form:
; RANK 3
;   (1)  (2)  (3)  (4)  (5)
;          C        B
;  AB.C  AB.  AB.  A.C  A.C
;               C        B
; RANK 4
;   (6)   (7)
;    C
;  AB.D  AB.D
;          C
;
; This is done with a soft look around strategy.
; For each grid element, search the surrounding cells.
;

$move_idx = 0	; Reset move list

for $i = $GRID_SZ_Y-1 to 0 step -1
	for $j = $GRID_SZ_X-1 to 0 step -1
		$x = $j
		$y = $i
		;ConsoleWrite("Scanning - " & $x & " " & $y & @CRLF)

		$special_flag = "N"	;reset special

		scan_north()
		scan_south()
		scan_east()
		scan_west()
	next
next

endfunc

func scan_north()
; Set up global vectors - used in all sub functions
$step_fwd_x = 0
$step_fwd_y = -1
$step_left_x = -1
$step_left_y = 0
$step_right_x = 1
$step_right_y = 0

if test_pattern_ab() then
	test_pattern1()
	test_pattern2()
	test_pattern3()
Else
	test_pattern45()
EndIf

endfunc

func scan_south()
; Set up global vectors - used in all sub functions
$step_fwd_x = 0
$step_fwd_y = 1
$step_left_x = 1
$step_left_y = 0
$step_right_x = -1
$step_right_y = 0

if test_pattern_ab() then
	test_pattern1()
	test_pattern2()
	test_pattern3()
Else
	test_pattern45()
EndIf

endfunc

func scan_east()
; Set up global vectors - used in all sub functions
$step_fwd_x = 1
$step_fwd_y = 0
$step_left_x = 0
$step_left_y = -1
$step_right_x = 0
$step_right_y = 1

if test_pattern_ab() then
	test_pattern1()
	test_pattern2()
	test_pattern3()
Else
	test_pattern45()
EndIf

endfunc

func scan_west()
; Set up global vectors - used in all sub functions
$step_fwd_x = -1
$step_fwd_y = 0
$step_left_x = 0
$step_left_y = 1
$step_right_x = 0
$step_right_y = -1

if test_pattern_ab() then
	test_pattern1()
	test_pattern2()
	test_pattern3()
Else
	test_pattern45()
EndIf

endfunc

func test_pattern_ab()
	$x1 = $x+$step_fwd_x
	$y1 = $y+$step_fwd_y
	if (0 <= $x1) and ($x1 < $GRID_SZ_X) and (0 <= $y1) and ($y1 < $GRID_SZ_Y) _
		and $grid[$x][$y] = $grid[$x1][$y1] Then

		if ($spec[$x][$y] = "F") or ($spec[$x1][$y1] = "F") Then
			$special_flag = "Y"
		endif

		return True
	Else
		return false
	EndIf
endfunc

func test_pattern1()
	$x1 = $x+($step_fwd_x*3)
	$y1 = $y+($step_fwd_y*3)
	if (0 <= $x1) and ($x1 < $GRID_SZ_X) and (0 <= $y1) and ($y1 < $GRID_SZ_Y) _
		and $grid[$x][$y] = $grid[$x1][$y1] Then

		if ($spec[$x1][$y1] = "F") or ($special_flag <> "N") then
			$rank = 5
		else
			$rank = 3
		endif

		push_move($x1, $y1, $x1-$step_fwd_x, $y1-$step_fwd_y, $rank)
	EndIf
EndFunc

func test_pattern2()
	$x1 = $x+($step_fwd_x*2)+$step_left_x
	$y1 = $y+($step_fwd_y*2)+$step_left_y
	if (0 <= $x1) and ($x1 < $GRID_SZ_X) and (0 <= $y1) and ($y1 < $GRID_SZ_Y) _
		and $grid[$x][$y] = $grid[$x1][$y1] Then

		;Test pattern 67 as well and upgrade move to a 4
		$x2 = $x+($step_fwd_x*3)
		$y2 = $y+($step_fwd_y*3)
		if (0 <= $x2) and ($x2 < $GRID_SZ_X) and (0 <= $y2) and ($y2 < $GRID_SZ_Y) _
		and $grid[$x][$y] = $grid[$x2][$y2] Then
			if ($spec[$x2][$y2] = "F") or ($special_flag <> "N") then
				$rank = 5
			else
				$rank = 4
			EndIf
		else
			if ($spec[$x1][$y1] = "F") or ($special_flag <> "N") then
				$rank = 5
			else
				$rank = 3
			endif
		endif

		push_move($x1, $y1, $x1+$step_right_x, $y1+$step_right_y, $rank)
	EndIf
EndFunc

func test_pattern3()
	$x1 = $x+($step_fwd_x*2)+$step_right_x
	$y1 = $y+($step_fwd_y*2)+$step_right_y
	if (0 <= $x1) and ($x1 < $GRID_SZ_X) and (0 <= $y1) and ($y1 < $GRID_SZ_Y) _
		and $grid[$x][$y] = $grid[$x1][$y1] Then

		;Test pattern 67 as well and upgrade move to a 4
		$x2 = $x+($step_fwd_x*3)
		$y2 = $y+($step_fwd_y*3)
		if (0 <= $x2) and ($x2 < $GRID_SZ_X) and (0 <= $y2) and ($y2 < $GRID_SZ_Y) _
		and $grid[$x][$y] = $grid[$x2][$y2] Then
			if ($spec[$x2][$y2] = "F") or ($special_flag <> "N") then
				$rank = 5
			else
				$rank = 4
			EndIf
		else
			if ($spec[$x1][$y1] = "F") or ($special_flag <> "N") then
				$rank = 5
			else
				$rank = 3
			endif
		endif

		push_move($x1, $y1, $x1+$step_left_x, $y1+$step_left_y, $rank)
	endif
EndFunc

func test_pattern45()
	$x1 = $x+($step_fwd_x*2)
	$y1 = $y+($step_fwd_y*2)
	if (0 <= $x1) and ($x1 < $GRID_SZ_X) and (0 <= $y1) and ($y1 < $GRID_SZ_Y) _
		and $grid[$x][$y] = $grid[$x1][$y1] Then

		;Test step left
		$x1 = $x+$step_fwd_x+$step_left_x
		$y1 = $y+$step_fwd_y+$step_left_y
		if (0 <= $x1) and ($x1 < $GRID_SZ_X) and (0 <= $y1) and ($y1 < $GRID_SZ_Y) _
			and $grid[$x][$y] = $grid[$x1][$y1] Then

			if ($spec[$x1][$y1] = "F") or ($special_flag <> "N") then
				$rank = 5
			Else
				$rank = 3
			endif

			push_move($x1, $y1, $x1+$step_right_x, $y1+$step_right_y, $rank)
		endif

		;Test step right
		$x1 = $x+$step_fwd_x+$step_right_x
		$y1 = $y+$step_fwd_y+$step_right_y
		if (0 <= $x1) and ($x1 < $GRID_SZ_X) and (0 <= $y1) and ($y1 < $GRID_SZ_Y) _
			and $grid[$x][$y] = $grid[$x1][$y1] Then

			if ($spec[$x1][$y1] = "F") or ($special_flag <> "N") then
				$rank = 5
			Else
				$rank = 3
			endif

			push_move($x1, $y1, $x1+$step_left_x, $y1+$step_left_y, $rank)
		endif
	EndIf
EndFunc

func push_move($fx, $fy, $tx, $ty, $rank)
	;ConsoleWrite("$move_idx = " & $move_idx & @CRLF)
	$move[$move_idx][0] = $fx
	$move[$move_idx][1] = $fy
	$move[$move_idx][2] = $tx
	$move[$move_idx][3] = $ty
	$move[$move_idx][4] = $rank
	$move_idx = $move_idx + 1
EndFunc

;Initialise the Game window
func init_game()

WinActivate($WINDOW)
If not WinActive($WINDOW) Then
	GUICtrlSetData($Grid_out, "No Game")
	return 1
EndIf

$hwnd = ControlGetHandle($WINDOW, "", $CONTROLID)
$pos = ControlGetPos($WINDOW, "", $CONTROLID)
$control_x = $pos[0] ; x
$control_y = $pos[1] ; y
return 0
EndFunc

func load_grid($s)
;Loads the grid with a test pattern

for $y = 0 to $GRID_SZ_Y-1			;run slow
	for $x = 0 to $GRID_SZ_X-1		;run fast
		$grid[$x][$y] = stringmid($s, (($y * 8) + $x)+1, 1)
	next
next

EndFunc

func print_grid()
;Prints the grid
$out = ""
for $y = 0 to $GRID_SZ_Y-1			;run slow
	for $x = 0 to $GRID_SZ_X-1		;run fast
		$out = $out & $grid[$x][$y]
	next
	$out = $out & @CRLF
next

GUICtrlSetData($Grid_out, $out)
EndFunc

func print_spec()
;Prints the specials
$out = ""
for $y = 0 to $GRID_SZ_Y-1			;run slow
	for $x = 0 to $GRID_SZ_X-1		;run fast
		$out = $out & $spec[$x][$y]
	next
	$out = $out & @CRLF
next

GUICtrlSetData($Spec_out, $out)
EndFunc

func print_move()
$out = ""
for $i = 0 to $move_idx
	$out = $out & $move[$i][0] & _
		" " & $move[$i][1] & _
		" " & $move[$i][2] & _
		" " & $move[$i][3] & _
		" " & $move[$i][4] _
		& @CRLF
Next
msgbox(0, "Move List", $out)
EndFunc

func sample_grid()
;Sample off-centre pixel of each cell and report its color
;Sample from left to right, top to bottom

$bad_recon = 0
for $y = 0 to $GRID_SZ_Y-1			;run slow
	for $x = 0 to $GRID_SZ_X-1		;run fast
		$grid[$x][$y] = convert_color(Hex(get_colour($x, $y), 6))
		$spec[$x][$y] = convert_special(Hex(get_special($x, $y), 6))

		if $grid[$x][$y] = "?" Then
			$bad_recon = $bad_recon + 1
		endif

	next
next
EndFunc

func get_colour($x, $y)
	return PixelGetColor( $control_x + $PLAY_X + ($x * $CELL_WIDTH) + ($CELL_WIDTH/2) + $offset_x, _
							$control_y + $PLAY_Y + ($y * $CELL_HEIGHT) + ($CELL_HEIGHT/2) + $offset_y, $hwnd )
EndFunc

func get_special($x, $y)
	return PixelGetColor( $control_x + $PLAY_X + ($x * $CELL_WIDTH) + ($CELL_WIDTH/2), _
							$control_y + $PLAY_Y + ($y * $CELL_HEIGHT) + 2, $hwnd )	;flame pos
EndFunc

func convert_color($hval)
$s = ""
$s = set_range(StringMid($hval, 1, 1))
$s = $s & set_range(StringMid($hval, 3, 1))
$s = $s & set_range(StringMid($hval, 5, 1))

for $i = 0 to $MAX_COL-1
	if StringCompare ($color[$i][0], $s) = 0 then
		return  $color[$i][1]
		ExitLoop
	else
		if $i = $MAX_COL-1 Then
			logmsg("Badrecog: " & $s)
			return "?"
			ExitLoop
		endif
	endif
next
EndFunc

func convert_special($hval)
;Check flame as yellow special

;return convert_color($hval)

if convert_color($hval) = "O" Then
	return "F"
Else
	return "N"
endif
EndFunc

func set_range($in)
switch ($in)
	case "0" to "4"
		return "L"
	case "5" to "7"
		return "M"
	case "8", "9", "A" to "F"
		return "H"
EndSwitch
EndFunc
